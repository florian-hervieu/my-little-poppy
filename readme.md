# My Little Poppy API

## Install & Run 
`docker-compose up`

`docker exec -it MyLittlePoppy-API-v3 php ./vendor/bin/phpunit`

## Objects
### Friend
Properties :
- [string] name
- [string] type
- [int] friendship
- [array] tags
- [bool] eaten


## Endpoints

### GET /friend/list
Parameters : 
- optional [string] name
- optional [string] type
- optional [int] friendship
- optional [array] tags
- optional [bool] eaten

Lists all the friends from the db. If `eaten` parameter is omitted, its value will default to `false`


### POST /friend/create
Parameters : 
- REQUIRED [string] name : Name of the friend
- REQUIRED [string/enum] type : Type of 'entity' picked from UNICORN, GOD, HOOMAN, NOOB
- REQUIRED [int] friendship : Friendship level, between 0 and 100
- REQUIRED [array] tags : A list of tags attached to the new friend

Attempts to create a friend. Returns the newly created friend if success, a list of errors if an error occurs.


### POST /friend/update-friendship
Parameters : 
- REQUIRED [string] id : Identifier of the friend
- REQUIRED [int] friendship : Friendship level, between 0 and 100

Attempts to change friendship level of a friend. Will only work if the friend exists and is not a GOD


### POST /monster/summon
Parameters :
- optional [string] id : Identifier of the 'chosen one' (friend)

Summon the monster to eat a friend. If not ID is given, the monster will attempt to eat a random friend. The monster
can only eat HOOMAN and NOOB friends. GOD friend will trigger an error, UNICORN will survive the attack.

