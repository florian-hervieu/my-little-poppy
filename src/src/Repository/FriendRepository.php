<?php

namespace App\Repository;

use App\Document\Friend;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;

class FriendRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Friend::class);
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        $query = $this->createQueryBuilder();

        //-- Criteria
        if (array_key_exists('eaten', $criteria)) {
            $query->field('eaten')->equals((bool) $criteria['eaten']);
        } else {
            $query->field('eaten')->equals(false);
        }

        if (array_key_exists('name', $criteria)) {
            $query->field('name')->equals($criteria['name']);
        }

        if (array_key_exists('type', $criteria)) {
            $criteria['type'] = Friend::TYPES[$criteria['type']]??false;

            if ($criteria['type'] !== false) {
                $query->field('type')->equals($criteria['type']);
            }
        }

        if (array_key_exists('friendship', $criteria)) {
            $query->field('friendship')->equals($criteria['friendship']);
        }

        if (array_key_exists('tags', $criteria)) {
            if (!is_array($criteria['tags'])) {
                $criteria['tags'] = [$criteria['tags']];
            }
            foreach ($criteria['tags'] as $tag) {
                $query->field('tags')->equals($tag);
            }
        }

        //-- Order
        if ($orderBy) {
            if (!is_array($orderBy)) {
                $orderBy = [$orderBy];
            }

            $query->sort($orderBy[0], $orderBy[1]??'ASC');
        }

        //-- Limit
        if ($limit) {
            $query->limit($limit);
        }

        //-- Offset
        if ($offset) {
            $query->skip($offset);
        }

        return $query->getQuery()->toArray();
    }

    public function findOneRandom(): Friend
    {
        $query = $this->dm->createAggregationBuilder(Friend::class);
        $query->sample(1);

        $friendList = $query->hydrate(Friend::class)
            ->getAggregation()
            ->getIterator()
        ;

        return $friendList->current();
    }
}