<?php

namespace App\Service;

use App\Document\Friend;
use App\Exception\HydrateException;
use ArrayObject;

class FriendHelper
{
    /**
     * @throws HydrateException
     */
    public function createFromArray(array $data): Friend
    {
        $friend = new Friend();
        $errors = new ArrayObject();

        if (array_key_exists('name', $data)) {
            if (!empty($data['name'])) {
                $friend->setName($data['name']);
            } else {
                $errors->append('friend.name.invalid');
            }
        } else {
            $errors->append('friend.name.missing');
        }

        if (array_key_exists('type', $data)) {
            if (array_key_exists($data['type'], Friend::TYPES)) {
                $friend->setType(Friend::TYPES[$data['type']]);
            } else {
                $errors->append('friend.type.invalid');
            }
        } else {
            $errors->append('friend.type.missing');
        }

        if (array_key_exists('friendship', $data)) {
            $friendship = intval($data['friendship']);
            if ($friendship >= 0 && $friendship <= 100) {
                $friend->setFriendship($friendship);
            } else {
                $errors->append('friend.friendship.invalid');
            }
        } else {
            $errors->append('friend.friendship.missing');
        }

        if (array_key_exists('tags', $data)) {
            if (is_array($data['tags'])) {
                $friend->setTags(array_filter($data['tags']));
            } else {
                $errors->append('friend.tags.invalid');
            }
        } else {
            $errors->append('friend.tags.missing');
        }

        if ($errors->count()) {
            $exception = new HydrateException();
            $exception->attachErrors($errors);

            throw $exception;
        }

        return $friend;
    }
}