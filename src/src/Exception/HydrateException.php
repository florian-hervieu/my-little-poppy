<?php

namespace App\Exception;

use ArrayObject;
use Exception;

class HydrateException extends Exception
{
    private ArrayObject $errors;

    public function attachErrors(ArrayObject $errors)
    {
        $this->errors = $errors;
    }

    public function getErrors(): ArrayObject
    {
        return $this->errors;
    }
}