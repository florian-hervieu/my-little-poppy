<?php

namespace App\Controller;

use App\Exception\UnicornException;
use App\Repository\FriendRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route("/monster")]
class MonsterController extends AbstractController
{
    #[Route("/summon", name:"app.monster.summon", methods: ["POST"])]
    public function summon(Request $request, FriendRepository $friendRepository, DocumentManager $dm, TranslatorInterface $translator): JsonResponse
    {
        if ($request->request->get('id') !== null) {
            $friend = $friendRepository->find($request->request->get('id'));
        } else {
            $friend = $friendRepository->findOneRandom();
        }

        if ($friend === null) {
            return new JsonResponse([
                'errors' => [$translator->trans("friend.not-found")]
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $target = $friend->getEaten();
            $dm->flush();

            return new JsonResponse([
                'target' => $target
            ]);
        } catch (UnicornException $e) {
            return new JsonResponse([
                'message' => [$translator->trans($e->getMessage())]
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'errors' => [$translator->trans($e->getMessage())]
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}