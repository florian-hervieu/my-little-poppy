<?php

namespace App\Controller;

use App\Document\Friend;
use App\Exception\HydrateException;
use App\Repository\FriendRepository;
use App\Service\FriendHelper;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route("/friend")]
class FriendController extends AbstractController
{
    #[Route("/create", name:"app.friend.create", methods: ["POST"])]
    public function create(Request $request, FriendHelper $friendHelper, DocumentManager $dm, TranslatorInterface $translator): JsonResponse
    {
        try {
            $friend = $friendHelper->createFromArray($request->request->all());

            $dm->persist($friend);
            $dm->flush();
        } catch (HydrateException $hydrateException) {
            $errors = $hydrateException->getErrors()->getArrayCopy();
            array_walk($errors, function(&$error) use ($translator) { $error = $translator->trans($error); });
            return new JsonResponse([
                'errors' => $errors
            ], 400);
        } catch (MongoDBException) {
            return new JsonResponse([
                'error' => $translator->trans('app.error.internal')
            ], 500);
        }

        return new JsonResponse([
            'friend' => $friend
        ]);
    }

    #[Route("/list", name:"app.friend.list", methods: ["GET"])]
    public function list(Request $request, FriendRepository $friendRepository, TranslatorInterface $translator): JsonResponse
    {
        $filters = array_intersect_key($request->query->all(), ['name' => 1, 'type' => 1, 'friendship' => 1, 'tags' => 1, 'eaten' => 1]);

        $friendList = $friendRepository->findBy($filters);

        if (empty($friendList) && (bool) $filters['eaten'] === true) {
            return new JsonResponse([
                'message' => $translator->trans('friend.list.no-casualties')
            ]);
        }

        return new JsonResponse([
            'friends' => $friendList
        ]);
    }

    #[Route("/update-friendship", name:"app.friend.update-friendship", methods: ["POST"])]
    public function updateFriendship(Request $request, FriendRepository $friendRepository, TranslatorInterface $translator): JsonResponse
    {
        $errors = [];
        if ($request->request->get('id') === null) {
            $errors[] = sprintf($translator->trans('app.error.required'), 'id');
        }
        if ($request->request->get('friendship') === null) {
            $errors[] = sprintf($translator->trans('app.error.required'), 'friendship');
        }

        if (!empty($errors)) {
            return new JsonResponse([
                'errors' => $errors
            ], Response::HTTP_BAD_REQUEST);
        }

        /** @var Friend $friend */
        $friend = $friendRepository->find($request->request->get('id'));

        if ($friend === null) {
            return new JsonResponse([
                'errors' => [$translator->trans("friend.not-found")]
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $friend->changeFriendship($request->request->getInt('friendship'));
        } catch (Exception $e) {
            return new JsonResponse([
                'errors' => [$translator->trans($e->getMessage())]
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'friend' => $friend
        ]);
    }
}