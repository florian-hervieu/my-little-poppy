<?php

namespace App\Document;

use App\Exception\UnicornException;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

/**
 * @MongoDB\Document(repositoryClass="FriendRepository::class")
 */
class Friend implements JsonSerializable
{
    public const TYPE_UNICORN = 0;
    public const TYPE_GOD     = 1;
    public const TYPE_HOOMAN  = 2;
    public const TYPE_NOOB    = 3;
    public const TYPES        = [
        'UNICORN' => self::TYPE_UNICORN,
        'GOD'     => self::TYPE_GOD,
        'HOOMAN'  => self::TYPE_HOOMAN,
        'NOOB'    => self::TYPE_NOOB,
    ];

    /**
     * @MongoDB\Id
     */
    private string $id;
    /**
     * @MongoDB\Field(type="string")
     */
    private string $name;
    /**
     * @MongoDB\Field(type="int")
     */
    private int $type;
    /**
     * @MongoDB\Field(type="int")
     */
    private int $friendship;
    /**
     * @MongoDB\Field(type="collection")
     */
    private array $tags;
    /**
     * @MongoDB\Field(type="bool")
     */
    private bool $eaten = false;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Friend
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Friend
    {
        $this->name = $name;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeLabel(): string
    {
        return array_search($this->type, self::TYPES);
    }

    public function setType(int $type): Friend
    {
        $this->type = $type;
        return $this;
    }

    public function getFriendship(): int
    {
        return $this->friendship;
    }

    /**
     * @throws Exception
     */
    public function changeFriendship(int $friendship): bool
    {
        if ($this->getType() === Friend::TYPE_GOD) {
            throw new Exception("friend.friendship.isgod");
        }

        $this->setFriendship($friendship);

        return true;
    }

    public function setFriendship(int $friendship): Friend
    {
        $this->friendship = $friendship;
        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): Friend
    {
        $this->tags = $tags;
        return $this;
    }

    public function isEaten(): bool
    {
        return $this->eaten;
    }

    public function setEaten(bool $eaten): Friend
    {
        $this->eaten = $eaten;
        return $this;
    }

    /**
     * @throws Exception|UnicornException
     */
    public function getEaten(): bool
    {
        if ($this->getType() === Friend::TYPE_GOD) {
            throw new Exception("monster.target.isgod");
        }

        if ($this->getType() === Friend::TYPE_UNICORN) {
            throw new UnicornException("monster.target.isunicorn");
        }

        $this->setEaten(true);

        return true;
    }

    #[ArrayShape(['id' => "string", 'name' => "string", 'type' => "string", 'friendship' => "int", 'tags' => "array", 'alive' => "bool"])]
    #[Pure]
    public function jsonSerialize(): array
    {
        return [
            'id'         => $this->getId(),
            'name'       => $this->getName(),
            'type'       => $this->getTypeLabel(),
            'friendship' => $this->getFriendship(),
            'tags'       => $this->getTags()
        ];
    }
}