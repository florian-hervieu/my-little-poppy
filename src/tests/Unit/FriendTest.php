<?php

namespace App\Tests\Unit;

use App\Document\Friend;
use App\Exception\HydrateException;
use App\Exception\UnicornException;
use App\Service\FriendHelper;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FriendTest extends KernelTestCase
{
    public function testCreateFromArrayValid()
    {
        self::bootKernel();

        $friendHelper = self::getContainer()->get(FriendHelper::class);

        $inputData = [
            'name' => 'Best Friend',
            'type' => 'HOOMAN',
            'friendship' => 98,
            'tags' => [
                'first tag',
                'second tag',
                'third tag'
            ]
        ];

        $friend = $friendHelper->createFromArray($inputData);

        $this->assertInstanceOf(Friend::class, $friend);
    }

    public function testCreateFromArrayMissingData()
    {
        self::bootKernel();

        $friendHelper = self::getContainer()->get(FriendHelper::class);

        $inputData = [];

        try {
            $friendHelper->createFromArray($inputData);
        } catch (HydrateException $exception) {
            $this->assertContains('friend.name.missing', $exception->getErrors());
            $this->assertContains('friend.type.missing', $exception->getErrors());
            $this->assertContains('friend.friendship.missing', $exception->getErrors());

            $this->assertNotContains('friend.name.invalid', $exception->getErrors());
            $this->assertNotContains('friend.type.invalid', $exception->getErrors());
            $this->assertNotContains('friend.friendship.invalid', $exception->getErrors());
        }
    }

    public function testCreateFromArrayInvalidData()
    {
        self::bootKernel();

        $friendHelper = self::getContainer()->get(FriendHelper::class);

        $inputData = [
            'name' => '',
            'type' => 'friend4ever',
            'friendship' => 9000
        ];

        try {
            $friendHelper->createFromArray($inputData);
        } catch (HydrateException $exception) {
            $this->assertContains('friend.name.invalid', $exception->getErrors());
            $this->assertContains('friend.type.invalid', $exception->getErrors());
            $this->assertContains('friend.friendship.invalid', $exception->getErrors());

            $this->assertNotContains('friend.name.missing', $exception->getErrors());
            $this->assertNotContains('friend.type.missing', $exception->getErrors());
            $this->assertNotContains('friend.friendship.missing', $exception->getErrors());
        }
    }

    public function testSummonMonsterFriendIsGod()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("monster.target.isgod");

        self::bootKernel();

        $friend = new Friend();
        $friend->setType(Friend::TYPE_GOD);
        $friend->getEaten();
    }

    public function testSummonMonsterFriendIsUnicorn()
    {
        $this->expectException(UnicornException::class);
        $this->expectExceptionMessage("monster.target.isunicorn");

        self::bootKernel();

        $friend = new Friend();
        $friend->setType(Friend::TYPE_UNICORN);
        $friend->getEaten();
    }

    public function testSummonMonsterFriendIsEatable()
    {
        self::bootKernel();

        $friend = new Friend();
        $friend->setType(Friend::TYPE_NOOB);
        $friend->getEaten();
        $this->assertEquals(true, $friend->isEaten());

        $friend = new Friend();
        $friend->setType(Friend::TYPE_HOOMAN);
        $friend->getEaten();
        $this->assertEquals(true, $friend->isEaten());
    }

    public function testChangeFriendshipFriendIsGod()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("friend.friendship.isgod");

        self::bootKernel();

        $friend = new Friend();
        $friend->setType(Friend::TYPE_GOD);
        $friend->changeFriendship(100);
    }

    public function testChangeFriendshipFriendIsChangeable()
    {
        self::bootKernel();

        $friend = new Friend();
        $friend->setType(Friend::TYPE_HOOMAN);
        $friend->changeFriendship( 100);
        $this->assertEquals(100, $friend->getFriendship());

        $friend = new Friend();
        $friend->setType(Friend::TYPE_NOOB);
        $friend->changeFriendship( 100);
        $this->assertEquals(100, $friend->getFriendship());

        $friend = new Friend();
        $friend->setType(Friend::TYPE_UNICORN);
        $friend->changeFriendship(100);
        $this->assertEquals(100, $friend->getFriendship());
    }
}